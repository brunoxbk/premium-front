import axios from "axios";
import { getToken } from "../reducers/auth"

const api = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}`
});

api.interceptors.request.use(async config => {
  const token = getToken();
  config.headers.Authorization = token != null ? `Bearer ${token} ` : `Bearer `;
  return config;
});


//Create a Http Client using Axios. Further modifications in this layer can be done later like Authorization.

const post = (url = '', data = '', config = {}) => {
    return api.post(url, data, config)
}

const get = (url, params) => {
    return api(url, params)
}

const put = (url = '', data = '', config = {}) => {
    return api.put(url, data, config)
}

//Cannot contain a delete method - Cause delete is a keyword.

const del = (url = '', config = {}) => {
    return api.delete(url, config)
}

//Encapsulating in a JSON object

const HttpClient = {
    post,
    get,
    put,
    delete: del
}

export {HttpClient}