import { HttpClient } from "../../services/httpClient"
import { push } from "connected-react-router"

const BASE = '/servicetabs'
const namespace = 'servicetab'

export const TYPES = {
  FETCH_SUCCESS: `${namespace}/FETCH_SUCCESS`,
  FETCH_ERROR: `${namespace}/FETCH_ERROR`,
  GET_SUCCESS: `${namespace}/GET_SUCCESS`,
  GET_ERROR: `${namespace}/GET_ERROR`,
  CREATE_SUCCESS: `${namespace}/CREATE_SUCCESS`,
  CREATE_ERROR: `${namespace}/CREATE_ERROR`,
  UPDATE_SUCCESS: `${namespace}/UPDATE_SUCCESS`,
  UPDATE_ERROR: `${namespace}/UPDATE_ERROR`,
  REMOVE_SUCCESS: `${namespace}/REMOVE_SUCCESS`,
  REMOVE_ERROR: `${namespace}/REMOVE_ERROR`
}

export const fetch = (params) => dispatch => {
  HttpClient.get(BASE, { params }).then(res => {
    dispatch(fetchSuccess(res.data))
  })
}

export const get = (id) => dispatch => {
  HttpClient.get(`${BASE}/${id}`).then(res => {
    dispatch(getSuccess(res.data))
  })
}

export const create = (data) => dispatch => {
  HttpClient.post(BASE, data).then(res => {
    dispatch(createSuccess(res.data))
    dispatch(push('/servicetabs'))
  })
}

export const update = (id, data) => dispatch => {
  HttpClient.update(`${BASE}/${id}`, data).then(res => {
    dispatch(updateSuccess(res.data))
    dispatch(push('/servicetabs'))
  })
}

export const remove = (item) => dispatch => {
  HttpClient.delete(`${BASE}/${item.id}`).then(res => {
    dispatch(removeSuccess(item.id))
  })
}

const removeSuccess = (item) => {
  return {type: TYPES.REMOVE_SUCCESS, payload: item}
}

const fetchSuccess = (itens) => {
  return { type: TYPES.FETCH_SUCCESS, payload: itens }
}

const getSuccess = (item) => {
  return { type: TYPES.GET_SUCCESS, payload: item }
}

const createSuccess = (item) => {
  return { type: TYPES.CREATE_SUCCESS, payload: item }
}

const updateSuccess = (item) => {
  return { type: TYPES.UPDATE_SUCCESS, payload: item }
}


const initialState = {
  servicetabs: [],
  servicetab: {clients: [], services: []},
  deleted: false
};

export function serviceTabReducer(state=initialState, action) {
  switch (action.type) {
    // The cases ordered in CRUD order.

    // Create
    case TYPES.CREATE_SUCCESS: {
      return { ...state, servicetab: action.payload }
    }

    // Get
    case TYPES.GET_SUCCESS: {
      return { ...state, servicetab: action.payload }
    }

    // Read
    case TYPES.FETCH_SUCCESS: {
      return { ...state, servicetabs: action.payload }
    }

    // Delete
    case TYPES.DELETE_SUCCESS: {
      return {
        ...state,
        servicetabs: state.servicetabs.filter((item) => item.id !== action.payload.id),
        deleted: true
      }
    }

    default:
      return state
  }
}