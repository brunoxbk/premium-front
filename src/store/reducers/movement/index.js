import { HttpClient } from "../../services/httpClient"
import { push } from "connected-react-router"

const BASE = '/movements'
const namespace = 'movement'

export const TYPES = {
  CREATE_SUCCESS: `${namespace}/CREATE_SUCCESS`,
  CREATE_ERROR: `${namespace}/CREATE_ERROR`,
  FETCH_SUCCESS: `${namespace}/FETCH_SUCCESS`,
  FETCH_ERROR: `${namespace}/FETCH_ERROR`,
  GET_SUCCESS: `${namespace}/GET_SUCCESS`,
  GET_ERROR: `${namespace}/GET_ERROR`,
  UPDATE_SUCCESS: `${namespace}/UPDATE_SUCCESS`,
  UPDATE_ERROR: `${namespace}/UPDATE_ERROR`,
  DELETE_SUCCESS: `${namespace}/DELETE_SUCCESS`,
  DELETE_ERROR: `${namespace}/DELETE_ERROR`,
}

export const fetch = (params) => dispatch => {
  HttpClient.get(BASE, {params}).then(res => {
    dispatch(fetchSuccess(res.data))
  })
}

export const get = (id) => dispatch => {
  HttpClient.get(`${BASE}/${id}`).then(res => {
    dispatch(getSuccess(res.data))
  })
}

export const create = (data) => dispatch => {
  HttpClient.post(BASE, data).then(res => {
    dispatch(createSuccess(res.data))
    dispatch(push('/movements'))
  })
}

export const update = (id, data) => dispatch => {
  HttpClient.put(`${BASE}/${id}`, data).then(res => {
    dispatch(updateSuccess(res.data))
    dispatch(push('/movements'))
  })
}

export const remove = (item) => dispatch => {
  HttpClient.del(item.id).then(res => {
    dispatch(removeSuccess(item.id))
  })
}

const removeSuccess = (movements) => {
  return {
    type: TYPES.DELETE_SUCCESS,
    payload: movements
  }
}

const fetchSuccess = (movements) => {
  return {
    type: TYPES.FETCH_SUCCESS,
    payload: movements
  }
}

const getSuccess = (movement) => {
  return {
    type: TYPES.GET_SUCCESS,
    payload: movement
  }
}

const createSuccess = (movement) => {
  return {
    type: TYPES.CREATE_SUCCESS,
    payload: movement
  }
}

const updateSuccess = (movement) => {
  return {
    type: TYPES.UPDATE_SUCCESS,
    payload: movement
  }
}

const initialState = {
  movements: [],
  movement: {},
  deleted: false
};

export function movementReducer(state=initialState, action) {
  switch (action.type) {
    // The cases ordered in CRUD order.

    // Create
    case TYPES.CREATE_SUCCESS: {
      return { ...state, movement: action.payload }
    }

    // Get
    case TYPES.GET_SUCCESS: {
      return { ...state, movement: action.payload }
    }

    // Read
    case TYPES.FETCH_SUCCESS: {
      return { ...state, movements: action.payload }
    }

    // Delete
    case TYPES.DELETE_SUCCESS: {
      return {
        ...state,
        movements: state.movements.filter((item) => item.id !== action.payload.id),
        deleted: true
      }
    }

    default:
      return state
  }
}

// The individual Reducer. It handles only one Todo Object.

// const todo = (state, action) => {

//   // If the mapped todo of the previous state matches with the new ID of the action,
//   // Only then proceed to the Reducer Switch case

//   if (state._id != (action._id || action.todo._id)) {
//       return state;
//   }

//   switch (action.type) {

//       // Edit/modifies the individual Todos using ES6 spread operator. The cases are self explanatory.

//       case TodoActions.START_EDITING:
//           {
//               return {
//                   ...state,
//                   editing: true
//               }
//           }

//       case TodoActions.CANCEL_EDITING:
//           {
//               return {
//                   ...state,
//                   editing: false
//               }
//           }

//       case TodoActions.UPDATE_TODO:
//           {
//               return {
//                   ...state,
//                   editing: false,
//                   updating: true
//               }
//           }

//       case TodoActions.UPDATE_TODO_SUCCESS:
//           {
//               return {
//                   ...state,
//                   ...action.todo,
//                   updating: false
//               }
//           }

//       case TodoActions.DELETE_TODO:
//           {
//               return {
//                   ...state,
//                   deleting: true
//               }
//           }

//       case TodoActions.DELETE_TODO_SUCCESS:
//           {
//               return false
//           }

//       default:
//           {
//               return state;
//           }
//   }
// }