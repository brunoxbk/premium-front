import { HttpClient } from "../../services/httpClient"

const BASE = '/reports'
const namespace = 'report'

export const TYPES = {
  FETCH_SUCCESS: `${namespace}/FETCH_SUCCESS`,
  FETCH_ERROR: `${namespace}/FETCH_ERROR`,
}

export const fetch = (params) => dispatch => {
  HttpClient.get(BASE, { params }).then(res => {
    dispatch(fetchSuccess(res.data))
  })
}

const fetchSuccess = (movements) => {
  return {
    type: TYPES.FETCH_SUCCESS,
    payload: movements
  }
}


const initialState = {
  data: [],
};

export function reportReducer(state=initialState, action) {
  switch (action.type) {
    // The cases ordered in CRUD order.

    // Read
    case TYPES.FETCH_SUCCESS: {
      return { ...state, data: action.payload }
    }

    default:
      return state
  }
}

// The individual Reducer. It handles only one Todo Object.

// const todo = (state, action) => {

//   // If the mapped todo of the previous state matches with the new ID of the action,
//   // Only then proceed to the Reducer Switch case

//   if (state._id != (action._id || action.todo._id)) {
//       return state;
//   }

//   switch (action.type) {

//       // Edit/modifies the individual Todos using ES6 spread operator. The cases are self explanatory.

//       case TodoActions.START_EDITING:
//           {
//               return {
//                   ...state,
//                   editing: true
//               }
//           }

//       case TodoActions.CANCEL_EDITING:
//           {
//               return {
//                   ...state,
//                   editing: false
//               }
//           }

//       case TodoActions.UPDATE_TODO:
//           {
//               return {
//                   ...state,
//                   editing: false,
//                   updating: true
//               }
//           }

//       case TodoActions.UPDATE_TODO_SUCCESS:
//           {
//               return {
//                   ...state,
//                   ...action.todo,
//                   updating: false
//               }
//           }

//       case TodoActions.DELETE_TODO:
//           {
//               return {
//                   ...state,
//                   deleting: true
//               }
//           }

//       case TodoActions.DELETE_TODO_SUCCESS:
//           {
//               return false
//           }

//       default:
//           {
//               return state;
//           }
//   }
// }