import { combineReducers } from 'redux'
import { userReducer } from './user'
import { commonReducer } from './common'
import { authReducer } from './auth'
import { groupReducer } from './group'
import { clientReducer } from './client'
import { serviceReducer } from './service'
import { serviceTabReducer } from './servicetab'
import { serviceSTabReducer } from './serviceservicetab'
import { movementReducer } from './movement'
import { reportReducer } from './report'
import { connectRouter } from 'connected-react-router'
import history from '../../routes/history'

const rootReducer = combineReducers({
  user: userReducer,
  common: commonReducer,
  auth: authReducer,
  group: groupReducer,
  servicetab: serviceTabReducer,
  client: clientReducer,
  movement: movementReducer,
  serviceSTab: serviceSTabReducer,
  service: serviceReducer,
  report: reportReducer,
  router: connectRouter(history)
})

export default rootReducer