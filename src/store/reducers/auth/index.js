import { HttpClient } from "../../services/httpClient"

export const SET_LOGIN_USER = 'SET_LOGIN_USER';
export const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR';
export const TOKEN_KEY = 'TOKEN_KEY';
export const SET_DATA_USER = 'SET_DATA_USER';
export const SET_DATA_USER_ERROR = 'SET_DATA_USER_ERROR';

export const isAuthenticated = () => localStorage.getItem(TOKEN_KEY) !== null

export const getToken = () => JSON.parse(localStorage.getItem(TOKEN_KEY))


export const login = data => dispatch => {
  HttpClient.post(data).then(res => {
    if (res.data.status === 200) {
      localStorage.setItem(TOKEN_KEY, JSON.stringify(res.data.user.id));
      localStorage.setItem("user", JSON.stringify(res.data.user));
      dispatch(loginSuccess(res.data.user, JSON.stringify(res.data.user.id)))
    } else {
      dispatch(loginError(res.data.message))
    }
  })
}

export const loginSuccess = (user, token) => {
  return {type: SET_LOGIN_USER, payload: {user, token}}
}

export const loginError = message => {
  return { type: SET_LOGIN_ERROR, payload: {message} }
}


const initialState = {
  user: JSON.parse(localStorage.getItem('user')),
  token: JSON.parse(localStorage.getItem(TOKEN_KEY)),
  message: '',
  profile: {
    pessoafisica_nome: "",
    email: "",
    telefone: ""
  }
}

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_USER:
      return {...state, user: action.payload.user, token: action.payload.token };
    case SET_LOGIN_ERROR:
      return {...state, message: action.payload.message };
    case SET_DATA_USER:
      return {...state, profile: action.payload.data };
    default:
      return state;
  }
};
