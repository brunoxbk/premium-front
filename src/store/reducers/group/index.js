import { HttpClient } from "../../services/httpClient"

import { push } from "connected-react-router"

const BASE = '/groups'

const namespace = 'group'

export const TYPES = {
  FETCH_SUCCESS: `${namespace}/FETCH_SUCCESS`,
  FETCH_ERROR: `${namespace}/FETCH_ERROR`,
  GET_SUCCESS: `${namespace}/GET_SUCCESS`,
  GET_ERROR: `${namespace}/GET_ERROR`,
  CREATE_SUCCESS: `${namespace}/CREATE_SUCCESS`,
  CREATE_ERROR: `${namespace}/CREATE_ERROR`,
  UPDATE_SUCCESS: `${namespace}/UPDATE_SUCCESS`,
  UPDATE_ERROR: `${namespace}/UPDATE_ERROR`,
  REMOVE_SUCCESS: `${namespace}/REMOVE_SUCCESS`,
  REMOVE_ERROR: `${namespace}/REMOVE_ERROR`
}

export const fetch = params => dispatch => {
  HttpClient.get(BASE, { params }).then(res => {
    dispatch(fetchSuccess(res.data))
  })
}

//nao precisa passa o return
export const get = id => dispatch => {
  HttpClient.get(`${BASE}/${id}`).then(res => {
    dispatch(getSuccess(res.data))
  })
}

export const create = (data) => dispatch => {
  HttpClient.post(BASE, data).then(res => {
    dispatch(createSuccess(res.data))
    dispatch(push('/groups'))
  })
}

export const update = (id, data) => dispatch => {
  HttpClient.put(`${BASE}/${id}`, data).then(res => {
    dispatch(updateSuccess(res.data))
    dispatch(push('/groups'))
  })
}

export const remove = (item) => dispatch => {
  HttpClient.delete(`${BASE}/${item.id}`).then(res => {
    dispatch(removeSuccess(item))
  })
}

const removeSuccess = (item) => {
  return { type: TYPES.REMOVE_SUCCESS, payload: item }
}

const fetchSuccess = (itens) => {
  return { type: TYPES.FETCH_SUCCESS, payload: itens }
}

const getSuccess = (item) => {
  return { type: TYPES.GET_SUCCESS, payload: item }
}

const createSuccess = (item) => {
  return { type: TYPES.CREATE_SUCCESS, payload: item }
}

const updateSuccess = (item) => {
  return { type: TYPES.UPDATE_SUCCESS, payload: item }
}


const initialState = {
  groups: [],
  group: {},
  deleted: false
};

export function groupReducer(state=initialState, action) {
  switch (action.type) {
    // The cases ordered in CRUD order.

    // Create
    case TYPES.CREATE_SUCCESS: {
      return { ...state, group: action.payload }
    }

    // Get
    case TYPES.GET_SUCCESS: {
      return { ...state, group: action.payload }
    }

    // Read
    case TYPES.FETCH_SUCCESS: {
      return { ...state, groups: action.payload }
    }

    // Delete
    case TYPES.REMOVE_SUCCESS: {
      return {
        ...state,
        groups: state.groups.filter((item) => item.id !== action.payload.id),
        deleted: true
      }
    }

    default:
      return state
  }
}

// The individual Reducer. It handles only one Todo Object.

// const todo = (state, action) => {

//   // If the mapped todo of the previous state matches with the new ID of the action,
//   // Only then proceed to the Reducer Switch case

//   if (state._id != (action._id || action.todo._id)) {
//       return state;
//   }

//   switch (action.type) {

//       // Edit/modifies the individual Todos using ES6 spread operator. The cases are self explanatory.

//       case TodoActions.START_EDITING:
//           {
//               return {
//                   ...state,
//                   editing: true
//               }
//           }

//       case TodoActions.CANCEL_EDITING:
//           {
//               return {
//                   ...state,
//                   editing: false
//               }
//           }

//       case TodoActions.UPDATE_TODO:
//           {
//               return {
//                   ...state,
//                   editing: false,
//                   updating: true
//               }
//           }

//       case TodoActions.UPDATE_TODO_SUCCESS:
//           {
//               return {
//                   ...state,
//                   ...action.todo,
//                   updating: false
//               }
//           }

//       case TodoActions.DELETE_TODO:
//           {
//               return {
//                   ...state,
//                   deleting: true
//               }
//           }

//       case TodoActions.DELETE_TODO_SUCCESS:
//           {
//               return false
//           }

//       default:
//           {
//               return state;
//           }
//   }
// }