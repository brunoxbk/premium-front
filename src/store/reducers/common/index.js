
const namespace = 'common'

export const SET_MESSAGE = `${namespace}/SET_MESSAGE`;
export const CLEAR_MESSAGE = `${namespace}/CLEAR_MESSAGE`;


export const setMessage = (message) => dispatch => {
  dispatch({ type: SET_MESSAGE, payload: message })
}

export const clearMessage = () => dispatch => {
  dispatch({ type: CLEAR_MESSAGE, payload: {class: 'is-invisible', text: ''}, })
}

const initialState = {
  message: {class: 'is-invisible', text: ''},
}

export const commonReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MESSAGE:
      return {...state, message: action.payload};
    case CLEAR_MESSAGE:
      return {...state, message: action.payload};
    default:
      return state;
  }
};
