import { HttpClient } from "../../services/httpClient"
import {get as getSt } from "../servicetab"

const namespace = 'serviceservicetab'

const BASE = `/serviceservicetabs`

export const TYPES = {
  CREATE_SUCCESS: `${namespace}/CREATE_SUCCESS`,
  FETCH_SUCCESS: `${namespace}/FETCH_SUCCESS`,
  REMOVE_SUCCESS: `${namespace}/REMOVE_SUCCESS`,
  REMOVE_ERROR: `${namespace}/REMOVE_ERROR`
}

export const fetch = (params) => dispatch => {
  HttpClient.get(BASE, { params }).then(res => {
    if (res.status === 200) {
      dispatch({ type: TYPES.FETCH_SUCCESS, payload: res.data })
    } else {
      console.log(res)
    }
  })
}

export const create = (data) => dispatch => {
  HttpClient.post(BASE ,data).then(res => {
    if (res.status === 200) {
      dispatch(fetch({service_tab_id: res.data.service_tab.id}))
      dispatch(getSt(res.data.service_tab.id))
      dispatch({ type: TYPES.CREATE_SUCCESS, payload: res.data })
    } else {
      console.log(res)
    }
  })
}

export const remove = (item) => dispatch => {
  HttpClient.delete(`${BASE}/${item.id}`).then(res => {
    dispatch(removeSuccess(item.id))
    dispatch(getSt(item.service_tab.id))
    dispatch(fetch({service_tab_id: item.service_tab.id}))
  })
}

const removeSuccess = (item) => {
  return {type: TYPES.REMOVE_SUCCESS, payload: item}
}

const initialState = {
  serviceservicetab: {},
  serviceservicetabs: [],
}

export const serviceSTabReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPES.CREATE_SUCCESS:
      return {...state, serviceservicetab: action.payload};
    case TYPES.FETCH_SUCCESS:
      return {...state, serviceservicetabs: action.payload};
    // Delete
    case TYPES.REMOVE_SUCCESS: {
      return {
        ...state,
        servicetabs: state.serviceservicetabs.filter((item) => item.id !== action.payload.id),
        deleted: true
      }
    }
    default:
      return state;
  }
};
