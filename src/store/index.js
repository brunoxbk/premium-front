import { createStore, applyMiddleware, compose } from 'redux'
import { connectRouter, routerMiddleware } from "connected-react-router";

import history from '../routes/history';
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from './reducers'

const loggerMiddleware = createLogger()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [
  thunkMiddleware,
  loggerMiddleware,
  routerMiddleware(history)
]

const store = createStore(
  connectRouter(history)(rootReducer),
  composeEnhancers(applyMiddleware(...middlewares))
)

export default store;