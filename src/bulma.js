// import Headroom from "@pluginjs/headroom"

export const initButton = () => {
  var burger = document.querySelector('.burger');
  var menu = document.querySelector('#'+burger.dataset.target);
  burger.addEventListener('click', function() {
      burger.classList.toggle('is-active');
      menu.classList.toggle('is-active');
  });
};

export const initDropProfile = () => {
  var dropdown = document.querySelector('.dropdown');
  dropdown.addEventListener('click', function(event) {
    event.stopPropagation();
    dropdown.classList.toggle('is-active');
  });
}

export const openModal = (openBtn, classModal, closeBtn) => {
  var btn = document.querySelector(openBtn);
  var btn1 = document.querySelector(closeBtn);
  var modal = document.querySelector(classModal);

  btn.addEventListener('click', function(event) {
    event.stopPropagation();
    modal.classList.toggle('is-active');
  });

  btn1.addEventListener('click', function(event) {
    event.stopPropagation();
    modal.classList.toggle('is-active');
  });
}

export const initNavbar = () => {
  var burger = document.querySelector('.burger');
  var item = document.querySelector('.nav-item');
  var nav = document.querySelector('#' + burger.dataset.target);

  //Bulma responsive nav
  burger.addEventListener('click', function(){
    burger.classList.toggle('is-active');
    nav.classList.toggle('is-active');
  });

  item.addEventListener('click', function(){
    console.log('click')
    burger.classList.toggle('is-active');
    nav.classList.toggle('is-active');
  });

  //Headroom (using Animate.css styles)
  // Headroom.of(header, {
  //   offset: 105,
  //   tolerance: 5,
  //   classes: {
  //     initial: "animated",
  //     pinned: "slideInDown",//custom: slideDown
  //     unpinned: "slideOutUp"//custom: slideUp
  //   }
  // });
  // headroom.init();
}