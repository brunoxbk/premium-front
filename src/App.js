import React from 'react';
import Routes from './routes';
import './styles/index.css';
import './styles/table.scss';
import './styles/novo.scss';

const App = () => <Routes />;
export default App;