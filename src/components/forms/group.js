import React from "react"
import { Form, Field } from "react-final-form"
import { Link } from 'react-router-dom'
import { requiredMessage, classInput, required } from './helpers'


const FormGroup = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="name" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Nome</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Nome" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <div className="field is-grouped is-pulled-right">
          <div className="control">
            <Link className="button is-danger" to='/groups'>
              <i className="fas fa-ban "></i> &nbsp;Cancelar
            </Link>
          </div>
          <div className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i className="fas fa-save"></i> &nbsp; Salvar
            </button>
          </div>
        </div>
      </form>
    )}/>
  </div>
)

export default FormGroup