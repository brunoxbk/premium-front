import React from "react";
import { Form, Field } from "react-final-form";
import { Link } from 'react-router-dom'
import { required, requiredMessage, classSelect, classInput } from "./helpers"

const FormServiceTab = (props) => (
  <div>
    <Form 
      initialValues={props.initials}
      onSubmit={props.onSubmit}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="client_id" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Cliente</label>
              <div className="control">
                <div className={classSelect(meta)}>
                  <select {...input}>
                    <option>Escolha um cliente</option>
                    {props.clients.map((client, index) => {
                      return (
                        <option key={index}  value={client.id}>{client.name}</option>
                      )})}
                  </select>
                </div>
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="date" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Data</label>
              <div className="control">
                <input {...input} min={props.min}
                  className={classInput(meta)} type="date" placeholder="Nome" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="time" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Time</label>
              <div className="control">
                <input {...input} className={classInput(meta)}
                  type="time" placeholder="Nome" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <div className="field is-grouped is-pulled-right">
          <div className="control">
            <Link className="button is-danger" to='/servicetabs'>
              <i className="fas fa-ban "></i> &nbsp;Cancelar
            </Link>
          </div>
          <div className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i className="fas fa-save"></i> &nbsp; Salvar
            </button>
          </div>
        </div>
      </form>
    )}/>
  </div>
)

export default FormServiceTab