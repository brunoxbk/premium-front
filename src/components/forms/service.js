import React from "react"
import { Form, Field } from "react-final-form"
import { Link } from 'react-router-dom'
import { requiredMessage, classInput, required } from './helpers'


const FormService = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="name" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Nome</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Nome" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="status" type="checkbox" >
          {({ input }) => (
            <div className="field">
              <label className="checkbox">
                <input type="checkbox" {...input} checked={true}/> Status
              </label>
            </div>
          )}
        </Field>
        <div className="field">
          <label className="label">Combo</label>
          <div className="select is-multiple">
            <Field name="services" component="select" multiple type='select' >
              {props.services.map((s, index) =>(
                <option key={index}  value={s.id}>{s.name} - {s.value}</option>
                )
              )}
            </Field>
          </div>
        </div>
        <Field name="value" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Valor</label>
              <div className="control">
                <input
                  {...input}
                  className={classInput(meta)}
                  type="number" step="0.5" placeholder="Valor" />
                  <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <div className="field is-grouped is-pulled-right">
          <div className="control">
            <Link className="button is-danger" to='/services'>
              <i className="fas fa-ban "></i> &nbsp;Cancelar
            </Link>
          </div>
          <p className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i className="fas fa-save"></i> &nbsp; Salvar
            </button>
          </p>
        </div>
      </form>
    )}/>
  </div>
)

export default FormService