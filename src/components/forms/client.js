import React from "react"
import { Form, Field } from "react-final-form"
import { masks } from "./helpers"
import formatString from "format-string-by-pattern"
import { Link } from 'react-router-dom'
import { requiredMessage, classInput, required } from './helpers'


const FormClient = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="name" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Nome</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Nome" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="phone" parse={formatString(masks.phone)} >
          {({ input }) => (
            <div className="field">
              <label className="label">Telefone</label>
              <div className="control">
                <input {...input} className='input' type="text" placeholder="Telefone" />
              </div>
            </div>
          )}
        </Field>
        <Field name="email">
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Email</label>
              <div className="control">
                <input {...input} className='input' type="text" placeholder="Email" />
              </div>
            </div>
          )}
        </Field>
        <Field name="cpf" parse={formatString(masks.cpf)} onFocus={(e) => console.log('foi')} >
          {({ input }) => (
            <div className="field">
              <label className="label">CPF</label>
              <div className="control">
                <input {...input} className='input' type="text" placeholder="CPF" />
              </div>
            </div>
          )}
        </Field>
        <Field name="birth" >
          {({ input }) => (
            <div className="field">
              <label className="label">Nascimento</label>
              <div className="control">
                <input {...input} className='input' type="date" placeholder="Nascimento" />
              </div>
            </div>
          )}
        </Field>
        <Field name="status" type="checkbox" >
          {({ input }) => (
            <div className="field">
              <label className="checkbox">
                <input type="checkbox" {...input}/> Status
              </label>
            </div>
          )}
        </Field>
        <div className="field is-grouped is-pulled-right">
          <div className="control">
            <Link className="button is-danger" to='/clients'>
              <i className="fas fa-ban "></i> &nbsp;Cancelar
            </Link>
          </div>
          <div className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i className="fas fa-save"></i> &nbsp; Salvar
            </button>
          </div>
        </div>
      </form>
    )}/>
  </div>
)

export default FormClient