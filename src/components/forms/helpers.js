import * as moment from 'moment'

export const classInput = meta => meta.error && meta.touched ? 'input is-danger' : 'input'

export const classSelect = meta => meta.error && meta.touched ? 'select is-danger' : 'select'

export const requiredMessage = meta => meta.error && meta.touched ? meta.error : ''

export const required = value => (value ? undefined : "Este campo é requerido!");

export const afterOrEqual = (value, allValue) => {
  const { start, end } = allValue
  return !value || moment(end).isSameOrAfter(start) ? undefined : 'Valor inválido'
};

export const beforeOrEqual = (value, allValue) => {
  const { start, end } = allValue

  return !value || moment(start).isSameOrBefore(end) ? undefined : 'Valor inválido'
};



export const masks = {
  phone: "(99) 99999-9999",
  cep: "12345-678",
  cpf: "XXX.XXX.XXX-XX",
  cnpj: "XX.XXX.XXX/XXXX-XX"
};