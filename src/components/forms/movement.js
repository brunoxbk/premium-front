import React from "react";
import { Form, Field } from "react-final-form";
import { required, classInput, requiredMessage, classSelect } from "./helpers"
import { Link } from 'react-router-dom'

const KindOptions = [
  {label: "Receita", value: "RE"},
  {label: "Despesa", value: "EX"}
]

const FormMovement = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="description" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Descrição</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Descrição" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="kind" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Tipo</label>
              <div className="control">
                <div className={classSelect(meta)}>
                  <select {...input}>
                    <option>Escolha um tipo</option>
                    {KindOptions.map((kind, index) => {
                      return (
                        <option key={index}  value={kind.value}>{kind.label}</option>
                      )})}
                  </select>
                </div>
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="date" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Data</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="date" placeholder="Data" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="value" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Valor</label>
              <div className="control">
                <input
                  {...input}
                  className={meta.error && meta.touched ? 'input is-danger' : 'input'}
                  type="number" step="0.5" placeholder="Valor" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>

        {/* <Field name="status" type="checkbox" >
          {({ input }) => (
            <div className="field">
              <label className="checkbox">
                <input type="checkbox" {...input} checked="true"/> Status
              </label>
            </div>
          )}
        </Field> */}
        <div className="field is-grouped is-pulled-right">
          <p className="control">
            <Link className="button is-danger" to='/movements'>
              <i className="fas fa-ban "></i> &nbsp;Cancelar
            </Link>
          </p>
          <p className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i className="fas fa-save"></i> &nbsp; Salvar
            </button>
          </p>
        </div>
      </form>
    )}/>
  </div>
)

export default FormMovement