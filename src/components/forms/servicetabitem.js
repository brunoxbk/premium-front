import React from "react";
import { Form, Field } from "react-final-form";
import { required, requiredMessage, classInput,classSelect } from "./helpers"

const FormServiceTabItem = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <div className="field is-horizontal">
          <Field name="service_id" validate={required} type='select'>
            {({ input, meta }) => (
              <div className="field is-horizontal">
                <div className="field">
                  <label className="label">Serviço</label>
                  <div className="control is-expanded">
                    <div className={classSelect(meta)}>
                      <select {...input} onChange={(e) => props.choiceService(e)} >
                        <option value='' >Escolha um serviço</option>
                        {props.services.map((service, index) => {
                          return (
                            <option key={index}  value={`${service.id}`}>{service.name}</option>
                          )})}
                      </select>
                    </div>
                    <p className="help is-danger">{requiredMessage(meta)}</p>
                  </div>
                </div>
              </div>
            )}
          </Field>
          &nbsp;
          &nbsp;
          <Field name="value" validate={required}>
            {({ input, meta }) => (
              <div className="field is-horizontal">
                <div className="field">
                  <label className="label">Valor</label>
                  <div className="control is-expanded">
                    <input
                      {...input} value={props.initials.value}
                      onChange={props.changeValue}
                      className={classInput(meta)} type="number" placeholder="Value" />
                    <p className="help is-danger">{requiredMessage(meta)}</p>
                  </div>
                </div>
              </div>
            )}
          </Field>
          &nbsp;
          &nbsp;
          <Field name="user_id" validate={required}>
            {({ input, meta }) => (
              <div className="field is-horizontal">
                <div className="field">
                  <label className="label">Colaborador</label>
                  <div className="control is-expanded">
                    <div className={classSelect(meta)}>
                      <select {...input}>
                        <option>Escolha um colaborador</option>
                        {props.users.map((user, index) => {
                          return (
                            <option key={index}  value={user.id}>{user.username}</option>
                          )})}
                      </select>
                    </div>
                    <p className="help is-danger">{requiredMessage(meta)}</p>
                  </div>
                </div>
              </div>
            )}
          </Field>
          &nbsp;  &nbsp; 
          <div className="field is-horizontal">
            <div className="field">
            <label className="label">&nbsp; </label>
              <div className="control">
                <button type="submit" disabled={submitting} className="button is-primary">
                  <i className="fas fa-plus"></i>  &nbsp; Adicionar Serviço
                </button>
              </div>
            </div>
          </div>

        </div>
      </form>
    )}/>
  </div>
)

export default FormServiceTabItem
