import React from "react";
import { Form, Field } from "react-final-form";

const required = value => (value ? undefined : "Este campo é requerido!");

const FormLogin = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="email" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Email</label>
              <div className="control">
                <input {...input} className={meta.error && meta.touched ? 'input is-danger' : 'input'} type="text" placeholder="Email" />
                <p className="help is-danger">{meta.error && meta.touched ? meta.error : ''}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="senha" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Senha</label>
              <div className="control">
                <input {...input} className={meta.error && meta.touched ? 'input is-danger' : 'input'} type="password" placeholder="Senha" />
                <p className="help is-danger">{meta.error && meta.touched ? meta.error : ''}</p>
              </div>
            </div>
          )}
        </Field>
        <div className="field is-grouped is-pulled-right">
          <div className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i class="fas fa-sign-in-alt"></i> &nbsp;Entrar
            </button>
          </div>
        </div>
      </form>
    )}/>
  </div>
)

export default FormLogin