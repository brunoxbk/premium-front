import React from "react"
import { Form, Field } from "react-final-form"
import { Link } from 'react-router-dom'
import { required, requiredMessage, classSelect, classInput } from "./helpers"
import { masks } from "./helpers"
import formatString from "format-string-by-pattern"


const FormUser = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <Field name="username" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Usuário</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Username" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="name" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Nome</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Nome" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="email" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Email</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="text" placeholder="Email" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="cpf" parse={formatString(masks.cpf)} >
          {({ input }) => (
            <div className="field">
              <label className="label">CPF</label>
              <div className="control">
                <input {...input} className='input' type="text" placeholder="CPF" />
              </div>
            </div>
          )}
        </Field>
        <Field name="phone" parse={formatString(masks.phone)} >
          {({ input }) => (
            <div className="field">
              <label className="label">Telefone</label>
              <div className="control">
                <input {...input} className='input' type="text" placeholder="Telefone" />
              </div>
            </div>
          )}
        </Field>
        <Field name="birth" >
          {({ input }) => (
            <div className="field">
              <label className="label">Nascimento</label>
              <div className="control">
                <input {...input} className='input' type="date" placeholder="Nascimento" />
              </div>
            </div>
          )}
        </Field>
        <Field name="status" type="checkbox" >
          {({ input }) => (
            <div className="field">
              <label className="checkbox">
                <input type="checkbox" {...input}/> Status
              </label>
            </div>
          )}
        </Field>
        <Field name="group_id" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Tipo</label>
              <div className="control">
                <div className={classSelect(meta)}>
                  <select {...input}>
                    <option>Escolha um grupo</option>
                    {props.groups.map((group, index) => {
                      return (
                        <option key={index}  value={group.id}> {group.name} </option>
                      )})}
                  </select>
                </div>
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <Field name="password" validate={required}>
          {({ input, meta }) => (
            <div className="field">
              <label className="label">Senha</label>
              <div className="control">
                <input {...input} className={classInput(meta)} type="password" placeholder="Senha" />
                <p className="help is-danger">{requiredMessage(meta)}</p>
              </div>
            </div>
          )}
        </Field>
        <div className="field is-grouped is-pulled-right">
          <div className="control">
            <Link className="button is-danger" to='/users'>
              <i className="fas fa-ban "></i> &nbsp;Cancelar
            </Link>
          </div>
          <div className="control">
            <button className="button is-primary" type="submit" disabled={submitting}>
              <i className="fas fa-save"></i> &nbsp;Salvar
            </button>
          </div>
        </div>
      </form>
    )}/>
  </div>
)

export default FormUser