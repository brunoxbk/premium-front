import React from "react";
import { Form, Field } from "react-final-form";
// import { classSelect, classInput } from "./helpers"

const FormServiceTabQuery = (props) => (
  <div>
    <Form onSubmit={props.onSubmit}
      initialValues={props.initials}
      render={({ handleSubmit, submitting }) => (
      <form onSubmit={handleSubmit}>
        <div className="field is-horizontal">
          <Field name="date">
            {({ input }) => (
              <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label">Data</label>
                </div>
                <div className="field-body">
                  <div className="control is-expanded">
                    <input {...input} className="input" type="date" placeholder="Data" />
                  </div>
                </div>
              </div>
            )}
          </Field>
          &nbsp;
          &nbsp;
          {/* <Field name="service_id">
            {({ input, meta }) => (
              <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label">Serviço</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <div className="control is-expanded">
                      <div className={classSelect(meta)}>
                        <select {...input}>
                          <option>Escolha um serviço</option>
                          {props.services.map((service, index) => {
                            return (
                              <option key={index}  value={service.id}>{service.name}</option>
                            )})}
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </Field>
          &nbsp;
          &nbsp;
          <Field name="user_id">
            {({ input, meta }) => (
              <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label">Colaborador</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <div className="control is-expanded">
                      <div className={classSelect(meta)}>
                        <select {...input}>
                          <option>Escolha um colaborador</option>
                          {props.users.map((user, index) => {
                            return (
                              <option key={index}  value={user.id}>{user.username}</option>
                            )})}
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </Field> */}
          &nbsp;
          &nbsp;
          <div className="field is-horizontal">
            <div className="field-label">
            </div>
            <div className="field-body">
              <div className="field">
                <div className="control">
                  <button type="submit" disabled={submitting} className="button is-primary">
                    <i className="fas fa-plus"></i>  &nbsp; Buscar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    )}/>
  </div>
)

export default FormServiceTabQuery
