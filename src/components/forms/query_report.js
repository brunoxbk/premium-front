import React from "react";
import { Form, Field } from "react-final-form";
import { classSelect, afterOrEqual, beforeOrEqual, classInput } from "./helpers"

const FormReport = (props) => (
  <Form onSubmit={props.onSubmit}
    initialValues={props.initials}
    render={({ handleSubmit, submitting, pristine }) => (
    <form onSubmit={handleSubmit}>
      <div className="field is-horizontal">
        <Field name="user_id">
          {({ input, meta }) => (
            <div className="field is-horizontal">
              <div className="field">
                <label className="label">Colaborador</label>
                <div className="control is-expanded">
                  <div className={classSelect(meta)}>
                    <select {...input}>
                      <option value="">Escolha um colaborador</option>
                      {props.users.map((user, index) => {
                        return (
                          <option key={index}  value={user.id}>{user.username}</option>
                        )})}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Field>
        &nbsp;
        &nbsp;
        <Field name="service_id">
          {({ input, meta }) => (
            <div className="field is-horizontal">
              <div className="field">
                <label className="label">Serviço</label>
                <div className="control is-expanded">
                  <div className={classSelect(meta)}>
                    <select {...input}>
                      <option value="">Escolha um serviço</option>
                      {props.services.map((service, index) => {
                        return (
                          <option key={index}  value={service.id}>{service.name}</option>
                        )})}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Field>
        &nbsp;
        &nbsp;
        <Field name="client_id">
          {({ input, meta }) => (
            <div className="field is-horizontal">
              <div className="field">
                <label className="label">Cliente</label>
                <div className="control is-expanded">
                  <div className={classSelect(meta)}>
                    <select {...input}>
                      <option value="">Escolha um cliente</option>
                      {props.clients.map((client, index) => {
                        return (
                          <option key={index}  value={client.id}>{client.name}</option>
                        )})}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Field>
      </div>
      <div className="field is-horizontal">
        <Field name="start" validate={(value, allValues) => afterOrEqual(value, allValues) }>
          {({ input, meta }) => (
            <div className="field is-horizontal">
              <div className="field">
                <label className="label">Inicio</label>
                <div className="control is-expanded">
                  <input {...input} className={classInput(meta)} type="date" placeholder="Data" />
                  <p className="help is-danger"><span>{meta.error}</span></p>
                </div>
              </div>
            </div>
          )}
        </Field>
        &nbsp;
        &nbsp;
        <Field name="end" validate={(value, allValues) => beforeOrEqual(value, allValues) }>
          {({ input, meta }) => (
            <div className="field is-horizontal">
              <div className="field">
                <label className="label">Fim</label>
                <div className="control is-expanded">
                  <input {...input} className={classInput(meta)} type="date" placeholder="Data" />
                  <p className="help is-danger"><span>{meta.error}</span></p>
                </div>
              </div>
            </div>
          )}
        </Field>
        <div className="field is-horizontal ">
          &nbsp;
          &nbsp;
          <div className="field-body">
            <div className="field">
            <label className="label">&nbsp;</label>
              <div className="control">
                <button type="submit"  disabled={submitting} className="button is-primary">
                  <i className="fas fa-search"></i>  &nbsp; Buscar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div className="field is-horizontal">
        
      </div>
    </form>
  )}/>
)

export default FormReport
