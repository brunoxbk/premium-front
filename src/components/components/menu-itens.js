export const menuItens = [
  {title: 'Início', patch: "/", icon: 'fa-home'},
  {title: 'Usuários', patch: "/users", icon: 'fa-user-friends'},
  {title: 'Grupos', patch: "/groups", icon: 'fa-users'},
  {title: 'Clientes', patch: "/clients", icon: 'fa-user-tie'},
  {title: 'Serviços', patch: "/services", icon: 'fa-briefcase '},
  {title: 'Movimentação', patch: "/movements", icon: 'fa-cash-register'},
  {title: 'Atendimentos', patch: "/servicetabs", icon: 'fa-id-card'},
  {title: 'Relatórios', patch: "/report", icon: 'fa-file-pdf'},
]