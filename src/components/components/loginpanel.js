import React from 'react';
import { Link } from 'react-router-dom';

const LoginPanel = () => {

  return (
    <div className="buttons">
      <Link to="/sign-up" className="button is-primary">
        <strong>Sign up</strong>
      </Link>
      <Link to="/login" className="button is-light">
        Login
      </Link>
    </div>
  );
}

export default LoginPanel;