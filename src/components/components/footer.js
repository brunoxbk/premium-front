import React from 'react'

const Footer = () => {
  return (
    <footer class="footer is-hidden">
      <div class="container">
        <div class="content has-text-centered">
          <p>Hello</p>
        </div>
      </div>
    </footer>
  )
}

export default Footer;