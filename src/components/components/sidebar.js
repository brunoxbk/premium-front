import React from 'react'
import { NavLink } from 'react-router-dom'
import {menuItens} from './menu-itens'
const Sidebar = () => {
  return (
    <aside className="column is-2 is-narrow-mobile is-fullheight section is-hidden-mobile">
      <p className="menu-labels is-hidden-touch">Navigation</p>
      <ul className="menu-list">
        {menuItens.map((item, index) => {
          return (
            <li key={`sidebar-${index}`}>
              <NavLink exact to={item.patch} activeClassName='is-active'>
                <span className="icon">
                  <i className={`fas ${item.icon}`}></i>
                </span>
                &nbsp;{item.title}
              </NavLink>
            </li>)
          })
        }
        {/* <li>
          <NavLink to="/link" className="is-active">
            <span className="icon"><i className="fas fa-table"></i></span> Links
          </NavLink>
          <ul>
            <li>
              <NavLink to="/link">
                <span className="icon is-small"><i className="fas fa-link"></i></span> Link1
              </NavLink>
            </li>
            <li>
              <NavLink to="#">
                <span className="icon is-small"><i className="fas fa-link"></i></span> Link2
              </NavLink>
            </li>
          </ul>
        </li>
        <li>
          <NavLink to="/" className="is-active">
            <span className="icon"><i className="fas fa-table"></i></span> Links
          </NavLink>
          <ul>
          </ul>
        </li>
        <li>
          <NavLink to="#" className="">
            <span className="icon"><i className="fas fa-info"></i></span> About
          </NavLink>
        </li> */}
      </ul>
    </aside>
  )
}

export default Sidebar;