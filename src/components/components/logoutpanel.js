import React from 'react';
import { Link } from 'react-router-dom';

const LogoutPanel = ({name}) => {
  return (
    <div className="buttons">
      <div className="dropdown">
        <div className="dropdown-trigger">
          <button className="button" aria-haspopup="true" aria-controls="dropdown-menu">
            <span>{name}</span>
            <span className="icon is-small">
              <i className="fas fa-angle-down" aria-hidden="true"></i>
            </span>
          </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
          <div className="dropdown-content">
            <Link to="/profile" className="dropdown-item is-active">
              Perfil
            </Link>
            <hr className="dropdown-divider" />
            <Link to="#" className="dropdown-item">
              Buscar Imóveis
            </Link>
            <Link to="#" className="dropdown-item">
              Listas
            </Link>
            <Link to="#" className="dropdown-item">
              Visitas
            </Link>
            <Link to="#" className="dropdown-item">
              Propostas
            </Link>
            <Link to="#" className="dropdown-item">
              Meu aluguel
            </Link>
            <Link to="#" className="dropdown-item">
              Ajuda
            </Link>
            <hr className="dropdown-divider" />
            <button className="button open-modal-logout dropdown-item is-light">
              Sair
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LogoutPanel;