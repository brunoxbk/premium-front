import React from 'react'

const Modal = (props) => {
  let classModal = `modal ${props.classModal ? props.classModal : ""}`;
  let classCancel = `button ${props.classCancel ? props.classCancel : ""}`;
  let classOK = `delete ${props.classClose ? props.classClose : ""}`;

  return (
    <div className={classModal}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{props.title}</p>
          <button className={classCancel} aria-label="close"></button>
        </header>
        <section className="modal-card-body">
          {props.content}
        </section>
        <footer className="modal-card-foot">
          <button className="button is-success" onClick={props.clickOk}>Ok</button>
          <button className={classCancel} aria-label="close">Cancel</button>
        </footer>
      </div>
    </div>
  );
}

export default Modal;


