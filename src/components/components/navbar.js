import React from 'react';
import { NavLink } from 'react-router-dom';
// import { getToken, isAuthenticated } from "./../../services/auth";
// import LoginPanel from './loginpanel';
// import LogoutPanel from './logoutpanel';
// import Modal from './modal';
// import {initDropProfile, openModal, initNavbar} from './../../bulma';
import {initNavbar} from './../../bulma';
import { connect } from 'react-redux';
import {menuItens} from './menu-itens'


class NavBar extends React.Component {
  // state = {user: {name: ''}};

  componentDidMount () {
    initNavbar();
    // if (this.props.token !== null) {
    //  initDropProfile();
    //  openModal('.open-modal-logout', '.modal-logout', '.modal-logout-close');
    // }
    // const user = getToken();
    // this.setState({ user });
  }

  render() {
    // let panel;
    // if (this.props.token !== null) {
    //   panel = <LogoutPanel name={this.props.user.name} />;
    // } else {
    //   panel = <LoginPanel  />;
    // }

    return (
      <nav className="navbar has-shadow ">
        <div className="container">
          <div className="navbar-brand">
            <NavLink to="/servicetabs" className="navbar-item">
              Premium
            </NavLink>
            <label className="navbar-burger burger" data-target="navMenu">
              <span></span>
              <span></span>
              <span></span>
            </label>
          </div>
          <input type="checkbox" id="menu-toggle" defaultChecked={false} className="is-hidden"/>
          <div className="navbar-menu" id="navMenu">
            <div className="navbar-end">
            {menuItens.map((item, index) => {
              return (
                <NavLink exact key={`menu-${index}`} to={item.patch}
                  activeClassName='is-active'
                  className="nav-item navbar-item is-tab is-hidden-tablet">
                  <span className="icon">
                    <i className={`fa ${item.icon}`}></i>
                  </span>
                  &nbsp;{item.title}
                </NavLink>
              )})}

              <NavLink to="/servicetabs" className="navbar-item is-tab is-active">
                <span className="icon"><i className="fa fa-user"></i></span>
              </NavLink>
              <NavLink to="/servicetabs" className="navbar-item is-tab">
                <span className="icon"><i className="fas fa-sign-out-alt"></i></span>
              </NavLink>
            </div>
          </div>
        </div>
      </nav>
    );
   }
}

const mapStateToProps = store => ({
  user: store.auth.user,
  token: store.auth.token
});

export default connect(mapStateToProps)(NavBar);
