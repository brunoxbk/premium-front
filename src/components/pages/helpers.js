

export const formatDate = date => new Date(date).toLocaleDateString()
export const formatMoney = value => (value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')