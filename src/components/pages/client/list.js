import React, {Component} from 'react'
import { connect } from 'react-redux'
import {fetch, remove} from '../../../store/reducers/client'
import { Link } from 'react-router-dom'

class ClientList extends Component {

  componentDidMount () {
    this.props.fetch({})
  }

  classLoading = () => {
    return this.props.clients ? '' : ''
  }

  handleRemove = item => {
    if (window.confirm(`Deseja remover o cliente ${item.name}?`)) {
      this.props.remove(item)
    }
  }

  render() {
    return (
      <div className="section">
        <div className="columns is-gapless is-clearfix">
          <div className="column"></div>
          <div className="column is-one-quarter">
            <Link to="/clients/add" className="button is-primary is-pulled-right">
              <i className="fas fa-plus"></i>  &nbsp; Adicionar
            </Link>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nome</th>
                      <th>Telefone</th>
                      <th>Email</th>
                      <th>Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className={this.props.clients.length > 0 ? 'is-invisible no-display' : ''}>
                      <th colSpan='5' >
                        <div className="has-text-centered">
                          <span className="button is-primary is-loading">Loading</span>
                        </div>
                      </th>
                    </tr>
                  {this.props.clients.map((client, index) => {
                    return (
                      <tr key={index} className={this.props.clients.length > 0 ? '' : 'is-invisible .no-display'}>
                        <td>{client.id}</td>
                        <td>{client.name}</td>
                        <td>{client.fone}</td>
                        <td>{client.email}</td>
                        <td>
                          <div className="field has-addons">
                            <p className="control">
                              <Link className="button is-primary is-small" to={`/clients/${client.id}/detail`}>
                                <span className="icon is-small">
                                  <i className="fas fa-eye"></i>
                                </span>
                                <span>Ver</span>
                              </Link>
                            </p>
                            <p className="control">
                              <Link className="button is-small" to={`/clients/${client.id}/edit`}>
                                <span className="icon is-small">
                                  <i className="fas fa-edit"></i>
                                </span>
                                <span>Editar</span>
                              </Link>
                            </p>
                            <p className="control">
                              <span
                                className="button is-danger is-small open-modal-delete"
                                onClick={()=> this.handleRemove(client)}
                                >
                                <span className="icon is-small">
                                  <i className="fas fa-trash"></i>
                                </span>
                                <span>Apagar</span>
                              </span>
                            </p>
                          </div>
                        </td>
                      </tr>
                    )})
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  clients: store.client.clients,
});

const mapDispatchToProps = dispatch => ({
  fetch: (params) => dispatch(fetch(params)),
  remove: (item) => dispatch(remove(item)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ClientList);
