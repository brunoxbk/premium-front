import React, {Component} from 'react';
import { connect } from 'react-redux';
import {get} from './../../../store/reducers/client'


class Detail extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.get(params.id)
  }

  render() {
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <div className="container">
                  <h1 className="title">
                    {this.props.client.name}
                  </h1>
                  <p>
                    <span className="has-text-weight-bold">Email:</span>
                    &nbsp;
                    {this.props.client.email}
                  </p>
                  <p>
                    <span className="has-text-weight-bold">CPF:</span>
                    &nbsp;
                    {this.props.client.cpf}
                  </p>
                  <p>
                    <span className="has-text-weight-bold">Fone:</span>
                    &nbsp;
                    {this.props.client.fone}</p>
                  <p>
                    <span className="has-text-weight-bold">Aniversário:</span>
                    &nbsp; {this.props.client.birth}
                  </p>
                </div>
              </div>
              <div className="column"></div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  client: store.client.client,
})

const mapDispatchToProps = {
  get,
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
