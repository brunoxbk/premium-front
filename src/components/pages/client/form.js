import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormClient from '../../forms/client'
import { create, get, update } from '../../../store/reducers/client'


class ClientForm extends Component {

  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    const { match: { params } } = this.props
    if ('id' in params) {
      this.props.get(params.id)
    }
  }

  handleSubmit = (data) => {
    const { match: { params } } = this.props

    if (params.id) {
      this.props.update(params.id, data)
    } else {
      this.props.create(data)
    }
  }

  render() {
    const { match: { params } } = this.props
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
              </div>
              <div className="column">
                <FormClient
                  initials={params.id ?  this.props.client : {}}
                  onSubmit={(data) => this.handleSubmit(data)} />
              </div>
              <div className="column">
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  client: store.client.client
});

const mapDispatchToProps = {
  create,
  update,
  get,
}

export default connect(mapStateToProps, mapDispatchToProps)(ClientForm);
