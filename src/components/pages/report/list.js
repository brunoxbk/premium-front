import React, {Component} from 'react'
import { connect } from 'react-redux'
import { fetch } from '../../../store/reducers/report'
import { formatMoney } from '../helpers';
import FormReport from '../../forms/query_report'
import * as moment from 'moment'

import {fetch as fetchUsers} from './../../../store/reducers/user'
import {fetch as fetchServices} from './../../../store/reducers/service'
import {fetch as fetchClients} from './../../../store/reducers/client'

class List extends Component {

  constructor(props) {
    super(props);

    this.state = {
      initials: {
        start: moment().format('YYYY-MM-DD'),
        end: moment().format('YYYY-MM-DD')
      }
    }

    this.handleQuery = this.handleQuery.bind(this);
  }

  componentDidMount () {
    this.props.fetchServices({})
    this.props.fetchUsers({})
    this.props.fetchClients({})
    this.props.fetch({date: [this.state.initials.start,this.state.initials.end]})
  }

  // handleQuery = (data) => {
  //   const bkp = Object.assign({}, data)
  //   this.props.fetch(data)
  //   this.setState({initials: bkp})
  // }

  handleQuery = (params) => {
    const bkp = Object.assign({}, params)
    const {start, end } = params
    delete params.start
    delete params.end
    params.date = [start, end]
    this.props.fetch(params)
    this.setState({initials: bkp})
  }

  render() {
    return (
      <div className="section">
        <div className="columns is-gapless is-clearfix">
          <div className="column">
            <FormReport
              onSubmit={(data) => this.handleQuery(data)}
              initials={this.state.initials}
              services={this.props.services}
              clients={this.props.clients}
              users={this.props.users}/>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Data</th>
                      <th>Serviço</th>
                      <th>Colaborador</th>
                      <th>Cliente</th>
                      <th>Valor (R$)</th>
                    </tr>
                  </thead>
                  <tbody>
                  {this.props.data.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td>{item.id}</td>
                        <td>{new Date(item.date).toLocaleDateString()}</td>
                        <td>{item.service_name}</td>
                        <td>{item.user_name}</td>
                        <td>{item.client_name}</td>
                        <td>{formatMoney(item.service_value)}</td>
                      </tr>
                    )})
                  }
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Total: {this.props.data ? formatMoney(this.props.data.reduce((total, item) => total + item.service_value, 0)) : 0.0 } </th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  data: store.report.data,
  users: store.user.users,
  services: store.service.services,
  clients: store.client.clients
});

const mapDispatchToProps = {
  fetch,
  fetchUsers,
  fetchServices,
  fetchClients,
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
