import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { fetch as fetchUsers } from '../../store/reducers/user'
import { fetch as fetchClients } from '../../store/reducers/client'
import * as moment from 'moment'

class Intro extends React.Component {

  componentDidMount () {
    this.props.fetchUsers({birth: moment().format('YYYY-MM-DD')})
    this.props.fetchClients({birth: moment().format('YYYY-MM-DD')})
  }

  render() {
    return (
      <div className="section">
        <div className="card">
          <div className="card-header">
            <p className="card-header-title"><i className="fas fa-tachometer-alt"></i> &nbsp; Dados</p></div>
            <div className="card-content">
            <div className="content">
            <nav className="level">
              <div className="level-item has-text-centered">
                <div>
                  <p className="heading">Tweets</p>
                  <p className="title">3,456</p>
                </div>
              </div>
              <div className="level-item has-text-centered">
                <div>
                  <p className="heading">Following</p>
                  <p className="title">123</p>
                </div>
              </div>
              <div className="level-item has-text-centered">
                <div>
                  <p className="heading">Followers</p>
                  <p className="title">456K</p>
                </div>
              </div>
              <div className="level-item has-text-centered">
                <div>
                  <p className="heading">Likes</p>
                  <p className="title">789</p>
                </div>
              </div>
            </nav>
            </div>
          </div>
          </div>
          <br />

        <div className="card is-hidden1">
          <div className="card-header">
            <p className="card-header-title"><i className="fas fa-birthday-cake"></i> &nbsp; Aniversários </p>
          </div>
          <div className="card-content">
            <div className="content">
              <div className="columns">
                <div className="column">
                  <span className="panel-block" to='/'>
                    <span className="panel-icon">
                      <i className="fas fa-gift"></i>
                    </span>
                    Colaboradores
                  </span>
                  {this.props.users.map((user, index) => {
                    return (
                      <Link key={`user-${index}`} className="panel-block" to='/'>
                        <span className="panel-icon">
                          <i className="fas fa-gift"></i>
                        </span>
                        {user.name}
                      </Link>
                    )}
                  )}

                </div>
                <div className="column">
                  <span className="panel-block" to='/'>
                    <span className="panel-icon">
                      <i className="fas fa-gift"></i>
                    </span>
                    Clientes
                  </span>
                  {this.props.clients.map((client, index) => {
                    return (
                      <Link key={`client-${index}`} className="panel-block" to='/'>
                        <span className="panel-icon">
                          <i className="fas fa-gift"></i>
                        </span>
                        {client.name}
                      </Link>
                    )}
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />

        <div className="card is-hidden1">
          <div className="card-header">
            <p className="card-header-title"> <i className="fas fa-undo-alt"></i> &nbsp; Retornos</p>
          </div>
          <div className="card-content"><div className="content">Content</div></div>
        </div>
        <br />

        <div className="card is-hidden1">
          <div className="card-header"><p className="card-header-title">Header</p></div>
          <div className="card-content"><div className="content">Content</div></div>
        </div>
        <br />

        <div className="card is-hidden1">
          <div className="card-header"><p className="card-header-title">Header</p></div>
          <div className="card-content"><div className="content">Content</div></div>
        </div>
        <br />

        <div className="card is-hidden1">
          <div className="card-header"><p className="card-header-title">Header</p></div>
          <div className="card-content"><div className="content">Content</div></div>
        </div>
        <br />
      </div>
    );
  }
}

const mapStateToProps = store => ({
  users: store.user.users,
  clients: store.client.clients,
})

const mapDispatchToProps = {
  fetchUsers,
  fetchClients,
}

export default connect(mapStateToProps, mapDispatchToProps)(Intro);;
