import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormUser from '../../forms/user'
import { fetch as fetchGroup } from '../../../store/reducers/group'
import {update, get, create} from '../../../store/reducers/user'


class UserForm extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.fetchGroup({})
    this.props.get(params.id)
  }

  handleSubmit = (data) => {
    const { match: { params } } = this.props

    if (params.id) {
      this.props.update(params.id, data)
    } else {
      this.props.create(data)
    }
  }

  render() {

    return (
      <div className="section">
        <div className="container">
          <div className="columns">
            <div className="column">
              <div className="columns is-desktop">
                <div className="column"></div>
                <div className="column">
                  <FormUser
                    initials={this.props.user}
                    groups={this.props.groups}
                    onSubmit={(data) => this.handleSubmit(data)} />
                </div>
                <div className="column"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  groups: store.group.groups,
  user: store.user.user,
});

const mapDispatchToProps = {
  update,
  create,
  get,
  fetchGroup,
}

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
