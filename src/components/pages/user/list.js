import React, {Component} from 'react'
import { connect } from 'react-redux'
import { fetch, remove } from '../../../store/reducers/user'
import { Link } from 'react-router-dom'

class UserList extends Component {

  componentDidMount () {
    this.props.fetch({})
  }

  handleRemove = item => {
    if (window.confirm(`Deseja remover o usuário ${item.name}?`)) {
      this.props.remove(item)
    }
  }

  render() {

    return (
      <div className="section">
        <div className="columns is-gapless is-clearfix">
          <div className="column">
          </div>
          <div className="column is-one-quarter">
            <Link to="/users/add" className="button is-primary is-pulled-right">
              <i className="fas fa-plus"></i> &nbsp; Adicionar
            </Link>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                  {this.props.users.map((user, index) => {
                    return (
                      <tr key={index}>
                        <td>{user.id}</td>
                        <td>{user.username}</td>
                        <td>{user.email}</td>
                        <td>
                          <div className="field has-addons">
                            <p className="control">
                              <Link className="button is-primary is-small" to={`/users/${user.id}/detail`}>
                                <span className="icon is-small">
                                  <i className="fas fa-eye"></i>
                                </span>
                                <span>Ver</span>
                              </Link>
                            </p>
                            <p className="control">
                              <Link className="button is-small" to={`/users/${user.id}/edit`}>
                                <span className="icon is-small">
                                  <i className="fas fa-edit"></i>
                                </span>
                                <span>Editar</span>
                              </Link>
                            </p>
                            <p className="control">
                              <span className="button is-danger is-small"
                                onClick={() => this.handleRemove(user)}>
                                <span className="icon is-small">
                                  <i className="fas fa-trash"></i>
                                </span>
                                <span>Apagar</span>
                              </span>
                            </p>
                          </div>
                        </td>
                      </tr>
                    )})
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  users: store.user.users,
});


const mapDispatchToProps = {
  fetch,
  remove
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
