import React, {Component} from 'react';
import { connect } from 'react-redux';
import {get} from './../../../store/reducers/user'
import { formatMoney } from '../helpers'


class Detail extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.get(params.id)
  }

  render() {
    // const { servicetab, serviceservicetabs } = this.props;
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <div className="container">
                  <h1 className="title">
                    {this.props.user.name}
                  </h1>
                  <p>Email: {this.props.user.email}</p>
                  <p>Fone: {this.props.user.phone}</p>
                  <p>CPF: {this.props.user.cpf}</p>
                  <p>Nascimento: {this.props.user.birth}</p>
                  <p>Status: {this.props.user.status}</p>
                </div>
                <br/>
                <br/>
                <div className="container">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Service</th>
                      <th>Valor (R$)</th>
                      <th>Colaborador</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* {serviceservicetabs.map((item, index) => {
                      return (
                        <tr key={index} className={serviceservicetabs.length > 0 ? '' : 'is-invisible .no-display'}>
                          <td>{item.service.name}</td>
                          <td>{item.service_tab.id}</td>
                          <td>{formatMoney(item.service.value)}</td>
                          <td>{item.user.username}</td>
                        </tr>
                      )})} */}
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Total: 0.0 </td>
                      <td></td>
                    </tr>
                  </tbody>
                  </table>
                </div>
              </div>
              <div className="column"></div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  user: store.user.user,
})

const mapDispatchToProps = {
  get,
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
