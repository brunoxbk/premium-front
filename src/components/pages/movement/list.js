import React, {Component} from 'react'
import { connect } from 'react-redux'
import { fetch, remove } from '../../../store/reducers/movement'
import { Link } from 'react-router-dom'
import { formatDate, formatMoney } from '../helpers'

class MovementList extends Component {

  componentDidMount () {
    this.props.fetch({})
  }

  render() {
    return (
      <div className="section">
        <div className="columns is-gapless is-clearfix">
          <div className="column">
          </div>
          <div className="column is-one-quarter">
            <Link to="/movements/add" className="button is-primary is-pulled-right">
              <i className="fas fa-plus"></i> &nbsp; Adicionar
            </Link>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Descrição</th>
                      <th>Valor (R$)</th>
                      <th>Data</th>
                      <th>Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                  {this.props.movements.map((movement, index) => {
                    return (
                      <tr key={index}>
                        <td>{movement.id}</td>
                        <td>{movement.description}</td>
                        <td>{formatMoney(movement.value)}</td>
                        <td>{formatDate(movement.date)}</td>
                        <td>
                          <div className="field has-addons">
                            <p className="control">
                              <Link className="button is-link is-small" to={`/movements/${movement.id}/detail`}>
                                <span className="icon is-small">
                                  <i className="fas fa-eye"></i>
                                </span>
                                <span>Ver</span>
                              </Link>
                            </p>
                            <p className="control">
                              <Link className="button is-small" to={`/movements/${movement.id}/edit`}>
                                <span className="icon is-small">
                                  <i className="fas fa-edit"></i>
                                </span>
                                <span>Editar</span>
                              </Link>
                            </p>
                            <p className="control">
                              <span
                                className="button is-danger is-small"
                                onClick={() => this.props.removeMovement(movement)}>
                                <span className="icon is-small">
                                  <i className="fas fa-trash"></i>
                                </span>
                                <span>Apagar</span>
                              </span>
                            </p>
                          </div>
                        </td>
                      </tr>
                    )})
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  movements: store.movement.movements,
});

const mapDispatchToProps = {
  fetch,
  remove
}

export default connect(mapStateToProps, mapDispatchToProps)(MovementList);
