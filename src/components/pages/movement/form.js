import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormMovement from '../../forms/movement'
import { create, get, update } from '../../../store/reducers/movement'


class GroupForm extends Component {

  componentDidMount () {
    const { match: { params } } = this.props
    if ('id' in params) {
      this.props.get(params.id)
    }
  }

  handleSubmit = (data) => {
    const { match: { params } } = this.props

    if (params.id) {
      this.props.update(params.id, data)
    } else {
      this.props.create(data)
    }
  }

  render() {
    const { match: { params } } = this.props
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
              </div>
              <div className="column">
                <FormMovement
                  initials={params.id ?  this.props.client : {}}
                  onSubmit={(data) => this.handleSubmit(data)} />
              </div>
              <div className="column">
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  movement: store.movement.movement,
});

const mapDispatchToProps = {
  update,
  create,
  get,
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupForm);
