import React, {Component} from 'react';
import { connect } from 'react-redux';
import { get } from '../../../store/reducers/movement';


class Detail extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.get(params.id)
  }

  render() {
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <p>Descrição: {this.props.movement.description}</p>
                <p>Valor: {this.props.movement.value}</p>
                <p>Data: {this.props.movement.date}</p>
                <p>Tipo: {this.props.movement.kind}</p>
              </div>
              <div className="column"></div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  movement: store.movement.movement
})

const mapDispatchToProps = {
  get,
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
