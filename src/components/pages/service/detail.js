import React, {Component} from 'react';
import { connect } from 'react-redux';
import {get} from './../../../store/reducers/service'
import { formatMoney } from '../helpers'


class Detail extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.get(params.id)
  }

  render() {
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <div className="container">
                  <h1 className="title">
                    {this.props.service.name}
                  </h1>
                  <p>Valor: {this.props.service.value}</p>
                  <br/>
                  {this.props.service.services.map((s, index) => <p key={index}>{s.name}</p>)}
                </div>
              </div>
              <div className="column"></div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  service: store.service.service,
})

const mapDispatchToProps = {
  get,
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
