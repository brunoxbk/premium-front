import React, {Component} from 'react'
import { connect } from 'react-redux'
import { fetch, remove } from '../../../store/reducers/service'
import { Link } from 'react-router-dom'
import { formatMoney } from '../helpers'

class ServiceList extends Component {

  componentDidMount () {
    this.props.fetch({combo: true})
  }

  handleRemove = item => {
    if (window.confirm(`Deseja remover o serviço ${item.name}?`)) {
      this.props.remove(item)
    }
  }

  render() {

    return (
        <div className="section">
          <div className="columns is-gapless is-clearfix">
            <div className="column">
            </div>
            <div className="column is-one-quarter">
              <Link to="/services/add" className="button is-primary is-pulled-right">
                <i className="fas fa-plus"></i> &nbsp; Adicionar
              </Link>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <div className="columns is-desktop">
                <div className="column">
                  <table className="table is-fullwidth is-responsive">
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Valor (R$)</th>
                        <th>Ações</th>
                      </tr>
                    </thead>
                    <tbody>
                    {this.props.services.map((service, index) => {
                      return (
                        <tr key={index}>
                          <td>{service.id}</td>
                          <td>{service.name}</td>
                          <td>{formatMoney(service.value)}</td>
                          <td>
                            <div className="field has-addons">
                              <p className="control">
                                <Link className="button is-primary is-small" to={`/services/${service.id}/detail`}>
                                  <span className="icon is-small">
                                    <i className="fas fa-eye"></i>
                                  </span>
                                  <span>Ver</span>
                                </Link>
                              </p>
                              <p className="control">
                                <Link className="button is-small" to={`/services/${service.id}/edit`}>
                                  <span className="icon is-small">
                                    <i className="fas fa-edit"></i>
                                  </span>
                                  <span>Editar</span>
                                </Link>
                              </p>
                              <p className="control">
                                <span
                                  className="button is-danger is-small"
                                  onClick={() => this.handleRemove(service)}>
                                  <span className="icon is-small">
                                    <i className="fas fa-trash"></i>
                                  </span>
                                  <span>Apagar</span>
                                </span>
                              </p>
                            </div>
                          </td>
                        </tr>
                      )})
                    }
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = store => ({
  services: store.service.services,
});

const mapDispatchToProps = dispatch => ({
  fetch: (params) => dispatch(fetch(params)),
  remove: (params) => dispatch(remove(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceList);
