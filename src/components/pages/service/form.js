import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormService from '../../forms/service'
import {update, get, create, fetch} from '../../../store/reducers/service'


class ServiceForm extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    if ('id' in params) {
      this.props.get(params.id)
    }
    this.props.fetch({combo: false})
  }

  handleSubmit = (data) => {
    const { match: { params } } = this.props

    if (params.id) {
      this.props.update(params.id, data)
    } else {
      this.props.create(data)
    }
  }

  render() {
    const { match: { params } } = this.props
    const { service } = this.props
    service.services = service.services ? service.services.map((i) => i.id) : []

    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column"></div>
              <div className="column">
                <FormService
                  initials={params.id ?  this.props.service : {}}
                  services={this.props.services}
                  onSubmit={(data) => this.handleSubmit(data)} />
              </div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  service: store.service.service,
  services: store.service.services,
});

const mapDispatchToProps = {
  update,
  get,
  create,
  fetch,
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceForm);
