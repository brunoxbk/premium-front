import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormLogin from '../forms/login';
import {login} from './../../store/reducers/auth'


class Login extends Component {

  componentDidMount () {
    if (this.props.token !== null){
      this.props.history.push("/app")
    }
  }

  componentDidUpdate() {
    if (this.props.token !== null){
      this.props.history.push("/app")
    }
  }

  render() {

    let msg

    if (this.props.message) {
      msg = <div className="notification is-warning"><button className="delete"></button>{this.props.message}</div>
    }

    return (
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column">
              <div className="columns is-desktop">
                <div className="column">
                </div>
                <div className="column">
                  {msg}
                  <FormLogin onSubmit={(data) => this.props.login(data)} />
                </div>
                <div className="column">
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = store => ({
  token: store.auth.token,
  message: store.auth.message,
});

const mapDispatchToProps = {
  login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
