import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormGroup from '../../forms/group'
import {create, get, update} from '../../../store/reducers/group'


class GroupForm extends Component {

  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    const { match: { params } } = this.props
    if ('id' in params) {
      this.props.get(params.id)
    }
  }

  handleSubmit = (data) => {
    const { match: { params } } = this.props

    if (params.id) {
      this.props.update(params.id, data)
    } else {
      this.props.create(data)
    }
  }

  render() {
    const { match: { params } } = this.props
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
              </div>
              <div className="column">
                <FormGroup
                  initials={params.id ? this.props.group : {}}
                  onSubmit={(data) => this.handleSubmit(data)} />
              </div>
              <div className="column">
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  token: store.auth.token,
  message: store.auth.message,
  group: store.group.group,
});

const mapDispatchToProps = {
  create,
  update,
  get:get
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupForm);
