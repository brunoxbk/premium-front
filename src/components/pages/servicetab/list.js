import React, {Component} from 'react'
import { connect } from 'react-redux'
import { fetch as fetchUsers } from '../../../store/reducers/user'
import { get as getServices } from './../../../store/reducers/service'
import FormServiceTabQuery from '../../forms/query_service_tab'
import { fetch as fetchServiceTab, remove as removeServiceTab } from './../../../store/reducers/servicetab'
import { Link } from 'react-router-dom'
import * as moment from 'moment'

class AttendanceList extends Component {

  componentDidMount () {
    this.props.fetchUsers({})
    this.props.fetchServiceTab({date: moment().format('YYYY-MM-DD')})
  }

  render() {
    const { users, services, servicetabs } = this.props;
    return (
      <div className="section">
        <div className="columns is-gapless is-clearfix">
          <div className="column">
          </div>
          <div className="column is-one-quarter">
            <Link to="/servicetabs/add" className="button is-primary is-pulled-right">
              <i className="fas fa-plus"></i>  &nbsp; Adicionar
            </Link>
          </div>
        </div>
        <div className="columns is-gapless is-clearfix">
          <div className="column">
            <FormServiceTabQuery
              initials={{date: moment().format('YYYY-MM-DD')}}
              onSubmit={(data)=>this.props.fetchServiceTab(data)}
              users={users}
              services={services}
            />
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Cliente</th>
                      <th>Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                  {servicetabs.map((servicetab, index) => {
                    return (
                      <tr key={index}>
                        <td>{servicetab.id}</td>
                        <td>{servicetab.clients[0].name}</td>
                        <td>
                          <div className="field has-addons">
                            <p className="control">
                              <Link className="button is-link is-small" to={`/servicetabs/${servicetab.id}/detail`}>
                                <span className="icon is-small">
                                  <i className="fas fa-eye"></i>
                                </span>
                                <span>Ver</span>
                              </Link>
                            </p>
                            <p className="control">
                              <Link className="button is-small" to={`/servicetabs/${servicetab.id}/edit`}>
                                <span className="icon is-small">
                                  <i className="fas fa-edit"></i>
                                </span>
                                <span>Editar</span>
                              </Link>
                            </p>
                            <p className="control">
                              <Link className="button is-small" to={`/servicetabs/${servicetab.id}/manage`}>
                                <span className="icon is-small">
                                  <i className="fas fa-edit"></i>
                                </span>
                                <span>Gerenciar</span>
                              </Link>
                            </p>
                            <p className="control">
                              <span
                                className="button is-danger is-small"
                                onClick={() => this.props.removeServiceTab(servicetab)}>
                                <span className="icon is-small">
                                  <i className="fas fa-trash"></i>
                                </span>
                                <span>Apagar</span>
                              </span>
                            </p>
                          </div>
                        </td>
                      </tr>
                    )})
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  servicetabs: store.servicetab.servicetabs,
  services: store.service.services,
  users: store.user.users,
});

const mapDispatchToProps = {
  fetchUsers,
  getServices,
  fetchServiceTab,
  removeServiceTab
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceList);
