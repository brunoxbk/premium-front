import React, {Component} from 'react';
import { connect } from 'react-redux';
import {get as getServiceTab} from './../../../store/reducers/servicetab'
import {fetch as fetchServices} from './../../../store/reducers/service'
import {create as createSTab, fetch as fetchSTab } from './../../../store/reducers/serviceservicetab'
import { formatMoney } from '../helpers'


class Detail extends Component {

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.getServiceTab(params.id)
    this.props.fetchServices({})
    this.props.fetchSTab({service_tab_id: params.id})
  }

  render() {
    const { servicetab, serviceservicetabs } = this.props;
    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
                <div className="container">
                  <h1 className="title">
                    {servicetab.clients.length > 0 ? servicetab.clients[0].name : ''}
                  </h1>
                  <h2 className="subtitle">
                    {servicetab.date}
                  </h2>
                </div>
                <div className="container">
                <table className="table is-fullwidth is-responsive">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Service</th>
                      <th>Valor (R$)</th>
                      <th>Colaborador</th>
                    </tr>
                  </thead>
                  <tbody>
                    {serviceservicetabs.map((item, index) => {
                      return (
                        <tr key={index} className={serviceservicetabs.length > 0 ? '' : 'is-invisible .no-display'}>
                          <td>{item.service.name}</td>
                          <td>{item.service_tab.id}</td>
                          <td>{formatMoney(item.service.value)}</td>
                          <td>{item.user.username}</td>
                        </tr>
                      )})}
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Total: {servicetab.movement ? formatMoney(servicetab.movement.value) : 0.0 } </td>
                      <td></td>
                    </tr>
                  </tbody>
                  </table>
                </div>
              </div>
              <div className="column"></div>
              <div className="column"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  servicetab: store.servicetab.servicetab,
  serviceservicetabs: store.serviceSTab.serviceservicetabs,
})

const mapDispatchToProps = {
  getServiceTab,
  fetchServices,
  createSTab,
  fetchSTab
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
