import React, {Component} from 'react';
import { connect } from 'react-redux';
import FormServiceTab from '../../forms/servicetab'
import {create} from './../../../store/reducers/servicetab'
import {fetch as fetchClients} from './../../../store/reducers/client'
import * as moment from 'moment'


class ServiceTabForm extends Component {

  componentDidMount () {
    this.props.fetchClients()
  }

  render() {

    const today = moment().format('YYYY-MM-DD');
    const now = moment().format('HH:mm');

    return (
      <div className="section">
        <div className="columns">
          <div className="column">
            <div className="columns is-desktop">
              <div className="column">
              </div>
              <div className="column">
                <FormServiceTab
                  clients={this.props.clients}
                  initials={{date: today, time: now}} min={today}
                  onSubmit={(data) => this.props.create(data)} />
              </div>
              <div className="column">
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  clients: store.client.clients,
  message: store.auth.message,
});

const mapDispatchToProps = {
  create,
  fetchClients
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTabForm);
