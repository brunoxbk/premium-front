import React, {Component} from 'react'
import { connect } from 'react-redux'
import {get as getServiceTab} from './../../../store/reducers/servicetab'
import {fetch as fetchUsers} from './../../../store/reducers/user'
import {fetch as fetchServices} from './../../../store/reducers/service'
import {create as createSTab, fetch as fetchSTab, remove as removeSTab} from './../../../store/reducers/serviceservicetab'
import FormServiceTabItem from './../../forms/servicetabitem'
import { Link } from 'react-router-dom'
import { formatMoney } from '../helpers'


class ServiceTabManage extends Component {

  constructor() {
    super()
    this.state = {value: 0.0, service_id: ''}
    this.choiceService = this.choiceService.bind(this)
    this.changeValue = this.changeValue.bind(this)
  }

  componentDidMount () {
    const { match: { params } } = this.props;
    this.props.getServiceTab(params.id)
    this.props.fetchServices({combo: true})
    this.props.fetchUsers({})
    this.props.fetchSTab({service_tab_id: params.id})
  }

  choiceService = (event) => {
    let idFind = parseInt(event.target.value)
    let service = this.props.services.find(item => item.id === idFind)
    this.setState({value: service ? service.value : '', service_id: idFind})
  }

  changeValue = (e) => this.setState({value: e.target.value})

  handleRemove = item => {
    if (window.confirm(`Deseja remover o serviço ${item.service.name}?`)) {
      this.props.removeSTab(item)
    }
  }

  render() {
    const { servicetab, users, services, serviceservicetabs } = this.props;
    return (
      <div className="section">
        <section className="hero is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                {servicetab.clients.length > 0 ? servicetab.clients[0].name : ''}
              </h1>
              <h2 className="subtitle">
                {servicetab.date}
              </h2>
            </div>
          </div>
        </section>
        
        <br/>
        
        <div className="columns is-gapless is-clearfix">
          <div className="column">
            <FormServiceTabItem
              onSubmit={(data) => this.props.createSTab({service_tab_id: servicetab.id ,...data})}
              services={services}
              initials={this.state}
              choiceService={this.choiceService}
              changeValue={this.changeValue}
              users={users}/>
          </div>
        </div>
        
        <div className="columns">
          <div className="column">
          <table className="table is-fullwidth is-responsive">
            <thead>
              <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Service</th>
                <th>Valor (R$)</th>
                <th>Colaborador</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
            {serviceservicetabs.map((item, index) => {
                return (
                  <tr key={index} className={serviceservicetabs.length > 0 ? '' : 'is-invisible .no-display'}>
                    <td>{item.id}</td>
                    <td>{item.service.name}</td>
                    <td>{item.service_tab.id}</td>
                    <td>{formatMoney(item.service.value)}</td>
                    <td>{item.user.username}</td>
                    <td>
                      <div className="field has-addons">
                        <p className="control">
                          <span className="button is-link is-small">
                            <span className="icon is-small">
                              <i className="fas fa-eye"></i>
                            </span>
                            <span>Ver</span>
                          </span>
                        </p>
                        <p className="control">
                          <Link className="button is-small" to={`/clients/${item.id}/edit`}>
                            <span className="icon is-small">
                              <i className="fas fa-edit"></i>
                            </span>
                            <span>Editar</span>
                          </Link>
                        </p>
                        <p className="control">
                          <span
                            className="button is-danger is-small"
                            onClick={() => this.handleRemove(item)}>
                            <span className="icon is-small">
                              <i className="fas fa-trash"></i>
                            </span>
                            <span>Apagar</span>
                          </span>
                        </p>
                      </div>
                    </td>
                  </tr>
                )})
              }
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total: {servicetab.movement ? formatMoney(servicetab.movement.value) : 0.0 } </td>
                <td></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
        
    );
  }
}

const mapStateToProps = store => ({
  servicetab: store.servicetab.servicetab,
  serviceservicetabs: store.serviceSTab.serviceservicetabs,
  services: store.service.services,
  users: store.user.users,
});

const mapDispatchToProps = {
  getServiceTab,
  fetchUsers,
  fetchServices,
  createSTab,
  fetchSTab,
  removeSTab
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTabManage);
