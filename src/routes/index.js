import React from "react"
import { Route, Switch, Redirect } from "react-router-dom"
import { connect } from 'react-redux';
import { ConnectedRouter } from "connected-react-router"
import history from './history'
import { isAuthenticated } from "../store/reducers/auth"
import Intro from '../components/pages/intro'
import Login from '../components/pages/login'
import Navbar from '../components/components/navbar'
import Sidebar from '../components/components/sidebar'

import UserList from '../components/pages/user/list'
import UserForm from '../components/pages/user/form'
import UserDetail from '../components/pages/user/detail'

import GroupList from '../components/pages/group/list'
import GroupForm from '../components/pages/group/form'

import ClientList from '../components/pages/client/list'
import ClientForm from '../components/pages/client/form'
import ClientDetail from '../components/pages/client/detail'

import ServiceList from '../components/pages/service/list'
import ServiceForm from '../components/pages/service/form'
import ServiceDetail from '../components/pages/service/detail'

import MovementList from '../components/pages/movement/list'
import MovementForm from '../components/pages/movement/form'
import MovementDetail from '../components/pages/movement/detail'

import ServiceTabList from '../components/pages/servicetab/list'
import ServiceTabForm from '../components/pages/servicetab/form'
import ServiceTabDetail from '../components/pages/servicetab/detail'
import ServiceTabManage from '../components/pages/servicetab/manage'

import ListReport from '../components/pages/report/list'

import {setMessage, clearMessage } from '../store/reducers/common'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
      )
    }
  />
);

const Routes = (props) => (
  <ConnectedRouter history={history}>
    <div>
      <Navbar />
      <section className="main-content columns is-fullheight">
        {/* <div className={props.message.class}>
          <button onClick={() => props.clearMessage()} className="delete"></button>
          {props.message.text}
        </div> */}

        <Sidebar/>
        <div className="container column is-10">
          <Switch>
            <Route exact path="/" component={Intro} />
            <Route exact path="/login" component={Login} />

            <Route path="/users/:id/edit" component={UserForm} />
            <Route path="/users/:id/detail" component={UserDetail} />
            <Route path="/users/add" component={UserForm} />
            <Route path="/users" component={UserList} />

            <Route exact path="/clients/:id/detail" component={ClientDetail} />
            <Route exact path="/clients/:id/edit" component={ClientForm} />
            <Route exact path="/clients/add" component={ClientForm} />
            <Route exact path="/clients" component={ClientList} />

            <Route exact path="/groups/:id/edit" component={GroupForm} />
            <Route exact path="/groups/add" component={GroupForm} />
            <Route exact path="/groups" component={GroupList} />

            <Route exact path="/services/:id/detail" component={ServiceDetail} />
            <Route exact path="/services/:id/edit" component={ServiceForm} />
            <Route exact path="/services/add" component={ServiceForm} />
            <Route exact path="/services" component={ServiceList} />

            <Route exact path="/movements/:id/detail" component={MovementDetail} />
            <Route exact path="/movements/:id/edit" component={MovementForm} />
            <Route exact path="/movements/add" component={MovementForm} />
            <Route exact path="/movements" component={MovementList} />

            <Route exact path="/servicetabs/:id/manage" component={ServiceTabManage} />
            <Route exact path="/servicetabs/:id/edit" component={ServiceTabForm} />
            <Route exact path="/servicetabs/:id/detail" component={ServiceTabDetail} />
            <Route exact path="/servicetabs/add" component={ServiceTabForm} />
            <Route exact path="/servicetabs" component={ServiceTabList} />

            <Route exact path="/report" component={ListReport} />

            <PrivateRoute exact path="/app" component={() => <h1>App</h1>} />
            <Route path="*" component={() => <h1>Page not found</h1>} />
          </Switch>
        </div>
      </section>
    </div>
  </ConnectedRouter>
);

const mapStateToProps = store => ({
  message: store.common.message,
});

const mapDispatchToProps = {
  setMessage,
  clearMessage
}

export default  connect(mapStateToProps, mapDispatchToProps)(Routes);